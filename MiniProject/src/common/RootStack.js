import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';

// Screen
import Login from '../screen/Login';
import Register from '../screen/Register';
import SplashScreen from '../screen/SplashScreen';
import Maintab from '../screen/MainTab';
import DrawerContent from '../screen/Drawer';
import playVideo from '../common/playVideo';
import Review from '../screen/Review';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const RootStackScreen = ({navigation}) => (
  <Drawer.Navigator
    headerMode="none"
    initialRouteName="SplashScreen"
    drawerContent={props => <DrawerContent {...props} />}>
    <Drawer.Screen name="SplashScreen" component={SplashScreen} />
    <Drawer.Screen name="Login" component={Login} />
    <Drawer.Screen name="Register" component={Register} />
    <Drawer.Screen name="Maintab" component={Maintab} />
    <Drawer.Screen name="playVideo" component={playVideo} />
    <Drawer.Screen name="Review" component={Review} />
  </Drawer.Navigator>
);

export default RootStackScreen;
