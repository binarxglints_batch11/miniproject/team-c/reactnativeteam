import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TextInput,
  ActivityIndicator,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign';
import IconM from 'react-native-vector-icons/MaterialIcons';

import {IMAGE_URL} from '../constant/general';
import {color} from '../styles/default';
import Modal from 'react-native-modal';
import {getVideosAction} from '../action/movieAction';

const SectionMovie = props => {
  // console.log('result', props.data);

  const dispatch = useDispatch();

  // useEffect(() => {
  //   dispatch(getVideosAction());
  // }, []);

  const handleToMovieDetail = movie => {
    setModalVisibility(true);
    setselectedMovie(movie);
  };

  //
  const generateGenre = genre_ids => {
    let genresName = props.genres
      .filter(genre => {
        let idx = genre_ids.findIndex(id => id == genre.id);
        if (idx != -1) {
          return genre;
        }
      })
      .map(genreObj => genreObj.name);
    return genresName.map(gen => (
      <View style={{flexDirection: 'row', marginLeft: 4}}>
        <Text style={styles.genreText}>{gen}</Text>
      </View>
    ));
  };

  const renderPoster = ({item}) => (
    <TouchableOpacity onPress={() => handleToMovieDetail(item)}>
      <Image
        source={{
          uri: `${IMAGE_URL}${item.poster_path}`,
        }}
        style={styles.poster}
        horizontal
      />
    </TouchableOpacity>
  );
  const [modalVisibility, setModalVisibility] = useState(false);
  const [selectedMovie, setselectedMovie] = useState('');
  // console.log('selected', selectedMovie);

  return (
    <View style={styles.container}>
      <Text style={styles.titleCategory}>{props.title}</Text>
      <FlatList
        data={props.data}
        renderItem={renderPoster}
        keyExtractor={(item, index) => index}
        horizontal
        showsHorizontalScrollIndicator={false}
      />

      {/* MODAL */}

      <Modal
        visible={modalVisibility}
        onRequestClose={() => setModalVisibility(false)}
        onBackdropPress={() => setModalVisibility(false)}
        style={styles.view}>
        <View style={styles.modalContainer}>
          <View>
            <Image
              source={{
                uri: `${IMAGE_URL}${selectedMovie.poster_path}`,
              }}
              style={styles.itemImage}
            />
            <Text style={styles.rating}>
              ImDb 🌟 {selectedMovie.vote_average}
            </Text>
            {/* <AntDesign
              name="play"
              size={50}
              color="gray"
              style={{position: 'absolute', top: 100, right: 50}}
              // onPress={() =>
              //   props.navigation.navigate('playVideo', {
              //     videos,
              //   })
              // }
            /> */}
          </View>
          <View>
            <Text style={styles.title}>{selectedMovie.title}</Text>
            {selectedMovie && generateGenre(selectedMovie?.genre_ids)}
            <Text style={styles.overview}>{selectedMovie.overview}</Text>
            <View style={styles.rateThisContainer}>
              <Text
                style={styles.rateThis}
                onPress={_ =>
                  props.navigation.navigate('Review', {
                    movieId: selectedMovie.id,
                  })
                }>
                Review
              </Text>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 250,
  },
  modalContainer: {
    backgroundColor: color.white,
    borderColor: 'black',
    flexDirection: 'row',
    borderWidth: 2,
    borderColor: 'white',
    borderRadius: 5,
    width: '100%',
  },
  overview: {
    fontSize: 14,
    width: 180,
    left: 2,
    fontFamily: 'Roboto',
    letterSpacing: 0.5,
    textAlign: 'justify',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 17,
    color: color.black,
    width: 180,
    left: 2,
  },
  rating: {
    fontSize: 14.5,
    left: 5,
    color: color.black,
    textAlign: 'center',
    marginTop: 7,
    marginBottom: 4,
  },

  genreText: {
    color: 'black',
    fontFamily: 'sans-serif',
    fontWeight: 'bold',
    fontSize: 11,
  },
  titleCategory: {
    fontWeight: 'bold',
    fontSize: 20,
    color: color.white,
    left: 15,
  },
  rateThisContainer: {
    flexDirection: 'column',
  },
  rateThis: {
    backgroundColor: color.gray,
    marginTop: 8,
    borderBottomWidth: 1,
    width: 60,
  },
  textSpace: {
    justifyContent: 'space-between',
  },
  poster: {
    width: 150,
    height: 300,
    marginVertical: 5,
    marginHorizontal: 10,
    borderRadius: 5,
    resizeMode: 'cover',
  },
  itemImage: {
    marginTop: 5,
    marginRight: 5,
    height: 250,
    width: 150,
  },
  view: {
    justifyContent: 'center',
  },
  judulModal: {
    color: 'white',
    fontFamily: 'roboto',
    margin: 10,
    fontWeight: 'bold',
    fontSize: 25,
  },
  textModal: {
    color: 'white',
    fontFamily: 'roboto',
    margin: 10,
    fontWeight: 'bold',
    fontSize: 20,
  },
});

export default SectionMovie;
