import React from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';
import {color} from '../styles/default';

const CustomButton = props => (
  <TouchableOpacity style={styles.container} onPress={props.onPressButton}>
    <Text style={styles.title}>{props.title}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    height: 45,
    width: 124,
    backgroundColor: color.white,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 18,
    color: color.black,
    fontWeight: 'bold',
  },
});

export default CustomButton;
