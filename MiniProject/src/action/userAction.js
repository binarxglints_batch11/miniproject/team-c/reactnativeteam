import * as types from '../constant/actionTypes';
import {API_URL1} from '../constant/general';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const getRequest = () => ({
  type: types.GET_REQUEST,
});

const getUserDetailSuccess = userDetail => ({
  type: types.GET_USER_DETAIL_SUCCESS,
  payload: userDetail,
});

const getUserDetailFailure = error => ({
  type: types.GET_USER_DETAIL_FAILURE,
  error,
});

export const getUserDetailAction = () => {
  let url = `${API_URL1}/user/`;

  return async dispatch => {
    const token = await AsyncStorage.getItem('accessToken');
    const AuthStr = 'Bearer '.concat(token);
    try {
      const response = await axios.get(url, {
        headers: {Authorization: AuthStr},
      });
      console.log('this is run', response.data.data);
      dispatch(getUserDetailSuccess(response.data.data));
    } catch (error) {
      console.log('Error', error);
      dispatch(getUserDetailFailure(error));
    }
  };
};

// console.log('ini token', token);
// dispatch(getRequest());

// export const getUser = () => {
//   // let url = `${API_URL}/movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
//   let url = `${API_URL1}/user/`;

//   return async dispatch => {
//     // dispatch(getRequest());
//     const token = await AsyncStorage.getItem('accessToken');
//     console.log('ini token', token);
//     try {
//       // const response = await axios.get(url);
//       // dispatch(getPopularSuccess(response.data.results));
//     } catch (error) {
//       console.log('Error', error);
//       // dispatch(getPopularFailure(error));
//     }
//   };
// };

// export const getUserDetailAction1 = () => dispatch => {
//   let url = `${API_URL1}/user/`;

//   const token = AsyncStorage.getItem('accessToken')
//     .then(res => {
//       return res;
//     })
//     .catch();

//   console.log(token);

//   const AuthStr = 'Bearer '.concat(token);
//   axios
//     .get(url, {
//       headers: {Authorization: AuthStr},
//     })
//     .then(response => {
//       return dispatch(getUserDetailSuccess(response.data.data));
//     })
//     .catch(error => {
//       console.log(error.message);
//     });
// };
