import * as types from '../constant/actionTypes';

const initialState = {
  loading: false,
  userDetail: {},
};
// console.log('initialstateUserdetail', initialState.userDetail);

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_REQUEST:
      return Object.assign({}, state, {
        loading: true,
        // userDetail: {},
      });

    case types.GET_USER_DETAIL_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        userDetail: action.payload,
      });

    case types.GET_USER_DETAIL_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error,
      });

    default:
      return state;
  }
};

export default userReducer;
