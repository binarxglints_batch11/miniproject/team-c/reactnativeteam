export const color = {
  black: '#000000',
  yellow: '#FFE7AB',
  white: '#FFFFFF',
  grey: '#848282',
  pink: '#ff80ab',
};

export const style = {
  containerMargin: 15,
  paddingTextInput: 15,
  marginTextInput: 10,
  small: 5,
  medium: 10,
  large: 15,
};
