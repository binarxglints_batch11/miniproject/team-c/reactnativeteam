import {StyleSheet} from 'react-native';
import {color, style} from './default';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: color.black,
    padding: 25,
  },

  logo: {
    width: 100,
    height: 100,
    top: 56,
    left: 130,
  },
  logoText: {
    color: 'white',
    top: 56,
    left: 150,
  },
  forget: {
    backgroundColor: color.black,
    color: color.white,
    fontSize: 15,
    top: 150,
    textAlign: 'right',
  },
  dontHaveAccount: {
    backgroundColor: color.black,
    color: color.white,
    fontSize: 15,
    top: 300,
    textAlign: 'center',
  },
  email: {
    top: 80,
    left: 20,
    color: 'red',
    borderColor: 'red',
    borderWidth: 2,
  },
  password: {
    top: 100,
    left: 20,
  },
  input: {
    backgroundColor: color.black,
    color: color.white,
    fontSize: 15,
    borderBottomColor: color.white,
    borderBottomWidth: 1,
    top: 100,
  },

  buttonContainer: {
    marginVertical: 180,
    alignItems: 'center',
  },

  signUp: {
    fontSize: 18,
    color: color.white,
    fontWeight: 'bold',
  },
  splash: {
    width: 180,
    height: 180,
    top: 100,
    left: 90,
  },
  splashText: {
    color: 'white',
    top: 95,
    left: 120,
    fontSize: 25,
  },
  profileImage: {
    width: 100,
    height: 100,
    top: 56,
    left: 130,
    borderRadius: 20,
  },
});

export default styles;
