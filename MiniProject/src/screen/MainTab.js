import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import {color} from '../styles/default';

// Navigation Setting
const Tab = createMaterialBottomTabNavigator();
const HomeStack = createStackNavigator();

// Screen
import Home from './Home';
import Review from './Review';
import Profile from './Profile';

// Setting Each Stack Scrren
// const HomeStackScreen = ({navigation}) => (
//   <HomeStack.Navigator
//     screenOptions={{
//       headerStyle: {
//         backgroundColor: color.black,
//       },
//       headerTintColor: color.red,
//       fontWeight: 'bold',
//     }}>
//     <HomeStack.Screen
//       name="Home"
//       component={Home}
//       options={{
//         title: 'Netflix',
//         headerLeft: () => (
//           <Ionicons.Button
//             name="ios-menu"
//             size={25}
//             backgroundColor={color.black}
//             color={color.white}
//             onPress={() => navigation.openDrawer()}
//           />
//         ),
// headerRight: () => (
//   <Ionicons.Button
//     name="notifications-outline"
//     size={25}
//     backgroundColor={color.black}
//     color={color.white}
//   />
// ),
// }}
//     />
//   </HomeStack.Navigator>
// );

// const ReviewStackScreen = ({navigation}) => (
//   <ReviewStack.Navigator
//     screenOptions={{
//       headerStyle: {
//         backgroundColor: color.black,
//       },
//       headerTintColor: color.white,
//       fontWeight: 'bold',
//     }}>
//     <ReviewStack.Screen name="Coming Soon" component={Review} />
//   </ReviewStack.Navigator>
// );

// const ProfileStackScreen = ({navigation}) => (
//   <ProfileStack.Navigator
//     screenOptions={{
//       headerStyle: {
//         backgroundColor: color.black,
//       },
//       headerTintColor: color.white,
//       fontWeight: 'bold',
//     }}>
//     <ProfileStack.Screen name="Profile" component={Profile} />
//   </ProfileStack.Navigator>
// );

const MainTab = () => (
  <Tab.Navigator
    initialRouteName="Home"
    activeColor={color.black}
    inactiveColor={color.grey}
    barStyle={{
      backgroundColor: '#d81b60',
      paddingVertical: 1,
    }}>
    {/* <Tab.Screen
      name="Review"
      component={Review}
      options={{
        tabBarLabel: 'My Review',
        tabBarColor: color.black,
        tabBarIcon: ({color}) => (
          <MaterialIcons name="rate-review" color={color} size={25} />
        ),
      }}
    /> */}

    <Tab.Screen
      name="Home"
      component={Home}
      options={{
        tabBarLabel: 'Home',
        tabBarColor: color.black,
        tabBarIcon: ({color}) => (
          <MaterialIcons name="home-filled" color={color} size={25} on />
        ),
      }}
    />

    <Tab.Screen
      name="Profile"
      component={Profile}
      options={{
        tabBarLabel: 'Profile',
        tabBarColor: color.black,
        tabBarIcon: ({color}) => (
          <MaterialIcons name="account-box" color={color} size={25} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default MainTab;
