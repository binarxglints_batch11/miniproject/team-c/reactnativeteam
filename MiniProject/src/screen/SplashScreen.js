import React, {useEffect} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import LottieView from 'lottie-react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {getUserDetailAction} from '../action/userAction';
import {useDispatch} from 'react-redux';
import {getMovieBEAction} from '../action/movieAction';

// navigation.navigate('Login');

const SplashScreen = ({navigation}) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUserDetailAction());
    dispatch(getMovieBEAction());

    // dispatch(getPopularAction());
    // dispatch(getUser());

    setTimeout(() => {
      cekToken();
    }, 3000);
  }, []);

  const cekToken = async () => {
    const token = await AsyncStorage.getItem('accessToken');
    // console.log(token);

    if (token) {
      // getUserDetailAction();
      navigation.navigate('Maintab');
    } else {
      navigation.navigate('Login');
    }
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#000000',
      }}>
      <Image source={require('../img/logo.png')} style={Styles.logo} />
      <Text style={Styles.logoText}>Meomee TV</Text>
      <LottieView
        source={require('../img/splash.json')}
        autoPlay
        loop={false}
        speed={1.5}
      />
    </View>
  );
};

export default SplashScreen;

const Styles = StyleSheet.create({
  logo: {
    width: 150,
    height: 150,
    top: 80,
    left: 130,
  },
  logoText: {
    color: '#d81b60',
    fontSize: 22,
    left: 13,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 5,
    top: 80,
  },
});
