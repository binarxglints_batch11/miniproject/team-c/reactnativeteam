import React, {useState, useEffect} from 'react';
import {Text, View, Image, TextInput} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {getUserDetailAction} from '../action/userAction';

import CustomButton from '../common/customButton';
import styles from '../styles/login';

const Register = props => {
  const [nama, setNama] = useState(null);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [confirmPassword, setConfirmPassword] = useState(null);

  const handleRegister = () => {
    // AsyncStorage.setItem('email', email);
    // props.navigation.navigate('Login');
    console.log(nama);
    console.log(email);
    console.log(password);
    console.log(confirmPassword);

    axios({
      method: 'POST',
      url: 'https://team-c.gabatch11.my.id/user/signup',
      data: {
        email,
        password,
        confirmPassword,
        nama,
      },
    })
      .then(({data}) => {
        AsyncStorage.setItem('accessToken', data.token, err => {
          if (!err) {
            setEmail('');
            setPassword('');
            setConfirmPassword('');
            setNama('');

            props.navigation.navigate('Maintab');
          } else {
            alert(error.message);
          }
        });
      })
      .catch(err => {
        alert(err);
      });
  };
  const handleLogin = () => {
    setEmail('');
    setPassword('');
    setConfirmPassword('');
    setNama('');
    props.navigation.navigate('Login');
  };

  return (
    <View style={styles.container}>
      <Image source={require('../img/logo2.png')} style={styles.logo} />
      <Text style={styles.logoText}>Meomee</Text>
      <View>
        <TextInput
          value={nama}
          onChangeText={setNama}
          style={styles.input}
          placeholder="Name"
          placeholderTextColor="white"
        />
      </View>
      <View>
        <TextInput
          value={email}
          onChangeText={setEmail}
          style={styles.input}
          placeholder="Email"
          placeholderTextColor="white"
        />
      </View>
      <View>
        <TextInput
          value={password}
          onChangeText={setPassword}
          style={styles.input}
          placeholder="Password"
          placeholderTextColor="white"
          secureTextEntry
        />
      </View>
      <View>
        <TextInput
          onChangeText={setConfirmPassword}
          value={confirmPassword}
          style={styles.input}
          placeholder="Confirm Password"
          placeholderTextColor="white"
          secureTextEntry
        />
      </View>
      <Text style={styles.dontHaveAccount} onPress={() => handleLogin()}>
        Already Have an Account? Sign In
      </Text>
      <View style={styles.buttonContainer}>
        <CustomButton title="Sign Up" onPressButton={() => handleRegister()} />
      </View>
    </View>
  );
};

export default Register;
