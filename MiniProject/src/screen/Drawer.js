import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, StyleSheet} from 'react-native';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import {Avatar, Title, Caption, Drawer} from 'react-native-paper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {getUserDetailAction} from '../action/userAction';

const DrawerContent = props => {
  const dispatch = useDispatch();
  const userDetail = useSelector(state => state.user.userDetail);

  useEffect(() => {
    dispatch(getUserDetailAction());
  }, []);

  const removeToken = async () => {
    try {
      // const tokenBefore = await AsyncStorage.getItem('accessToken');
      // console.log(tokenBefore);
      await AsyncStorage.removeItem('accessToken');
      // const token = await AsyncStorage.getItem('accessToken');

      // console.log(token);

      props.navigation.navigate('Login');
    } catch (err) {
      console.log(err.message);
    }
  };

  // console.log('ini userdetail', userDetail);

  return (
    <View style={styles.drawerContent}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <View style={{flexDirection: 'row', marginTop: 15}}>
              <Avatar.Image source={require('../img/image4.png')} size={50} />
              <View style={{flexDirection: 'column', marginLeft: 15}}>
                <Title style={styles.title}>{userDetail.nama}</Title>
                <Caption style={styles.caption}>{userDetail.email}</Caption>
              </View>
            </View>
          </View>
          <Drawer.Section style={styles.drawerSection}>
            {/* <DrawerItem
              icon={({color, size}) => (
                <Ionicons name="ios-heart-outline" color={color} size={size} />
              )}
              label="Favourites"
              onPress={() => props.navigation.navigate('Favourite')}
            /> */}
            {/* <DrawerItem
              icon={({color, size}) => (
                <MaterialIcons name="rate-review" color={color} size={size} />
              )}
              label="My Review"
              onPress={() => props.navigation.navigate('Review')}
            /> */}
          </Drawer.Section>
        </View>
      </DrawerContentScrollView>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={({color, size}) => (
            <Ionicons name="ios-person-outline" color={color} size={size} />
          )}
          label="Edit Profile"
          onPress={() => props.navigation.navigate('Profile')}
        />
        <DrawerItem
          icon={({color, size}) => (
            <Ionicons name="ios-exit-outline" color={color} size={size} />
          )}
          label="Sign Out"
          onPress={() => removeToken()}
        />
      </Drawer.Section>
    </View>
  );
};

export default DrawerContent;

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1,
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});
