import React, {useEffect} from 'react';
import moment from 'moment';
import {IMAGE_URL} from '../constant/general';
import {useSelector, useDispatch} from 'react-redux';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  ImageBackground,
} from 'react-native';
import {getMovieReviewAction, getImagesAction} from '../action/movieAction';

const Review = ({route}) => {
  const dispatch = useDispatch();
  const reviews = useSelector(state => state.movies.reviews);
  const images = useSelector(state => state.movies.images);

  // console.log(reviews);

  useEffect(() => {
    dispatch(getMovieReviewAction(route.params?.movieId));
    dispatch(getImagesAction(route.params?.movieId));
  }, [route.params?.movieId]);

  const cardReview = ({item}) => {
    // console.log('run', item);
    return (
      <ImageBackground
        // source={{uri: `${IMAGE_URL}/${.backdrop_path}`}}
        style={styles.container}>
        <View style={styles.leftContainer}>
          {/* uri: !item.author_details?.avatar_path.includes('https') :
          item.author_details?.avatar_path.slice(1, 1000), */}
          <Image
            source={{
              uri: `${IMAGE_URL}${item.author_details?.avatar_path}`,
            }}
            style={{
              width: 90,
              height: 90,
              borderRadius: 100,
              resizeMode: 'cover',
              borderWidth: 1,
              borderColor: 'white',
            }}
          />
          <Text
            style={{
              color: 'white',
              fontWeight: 'bold',
              fontSize: 16,
              marginVertical: 10,
            }}>
            {item.author_details.rating ? item.author_details.rating : 0}
            <Text style={{fontWeight: 'normal'}}> /10 🌟 </Text>
          </Text>
        </View>
        <View style={styles.rightContainer}>
          <Text style={{color: 'white', fontWeight: 'bold', fontSize: 16}}>
            A review by {item.author_details?.username}
          </Text>
          <Text style={{fontSize: 12, color: 'white'}}>
            Written by {item.author_details?.username} on{' '}
            {moment(item.created_at).format('LL')}
          </Text>
          <Text style={{marginTop: 20, textAlign: 'justify', color: 'white'}}>
            {item.content.slice(0, 200)}{' '}
            <Text style={{textDecorationLine: 'underline', color: 'red'}}>
              .....
            </Text>
          </Text>
        </View>
      </ImageBackground>
    );
  };

  return (
    <View style={styles.container1}>
      <Text style={styles.title}>MeoMee TV</Text>
      <Text style={styles.title2}>(Reviews)</Text>

      {reviews.length > 0 ? (
        <FlatList
          data={reviews}
          renderItem={cardReview}
          keyExtractor={(item, index) => index}
          horizontal={false}
          showsVerticalScrollIndicator={false}
        />
      ) : (
        <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <Text style={{fontSize: 16, color: 'white'}}>
            There's no review for this movie.
          </Text>
        </View>
      )}
    </View>
  );
};

export default Review;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 10,
    borderWidth: 1,
    borderColor: '#d81b60',
    borderRadius: 10,
    marginVertical: 5,
  },
  title: {
    color: '#d81b60',
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 5,
  },
  title2: {
    color: '#d81b60',
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 5,
  },
  container1: {
    padding: 30,
    backgroundColor: 'black',
    flex: 1,
  },
  leftContainer: {
    flex: 3,
    alignItems: 'center',
  },
  rightContainer: {
    flex: 7,
  },
});
