import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  FlatList,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import CardGenre from '../common/CardGenre';
import SectionMovie from '../common/SectionMovie';
import {getUserDetailAction} from '../action/userAction';
import {
  getVideosAction,
  getMovieBEAction,
  getGenresAction,
  getPopularAction,
  getUpcomingAction,
  getNowPlayingAction,
  getTopRatedAction,
} from '../action/movieAction';
console.disableYellowBox = true;
const Home = props => {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.movies.loading);
  const genres = useSelector(state => state.movies.genres);
  const [isRefresh, setIsRefresh] = useState(false);
  const [selectedGenres, setSelectedGenres] = useState([]);
  const [searchTextInput, setSearchTextInput] = useState(null);
  const movies = [
    {
      title: 'Upcoming',
      data: useSelector(state => state.movies.upcoming),
    },
    {
      title: 'Popular',
      data: useSelector(state => state.movies.popular),
    },
    {
      title: 'Now Playing',
      data: useSelector(state => state.movies.nowPlaying),
    },
    {
      title: 'Top Rated',
      data: useSelector(state => state.movies.topRated),
    },
  ];

  // console.log('ini genre', genres);

  useEffect(() => {
    dispatch(getUserDetailAction());
    dispatch(getVideosAction());
    dispatch(getMovieBEAction());
    dispatch(getGenresAction());
    dispatch(getPopularAction());
    dispatch(getUpcomingAction());
    dispatch(getNowPlayingAction());
    dispatch(getTopRatedAction());
  }, [selectedGenres]);

  const searchByTextInput = movies => {
    let filteredMovies = movies.filter(movie =>
      movie.title.toLowerCase().includes(searchTextInput.toLowerCase()),
    );
    return filteredMovies;
  };

  const handleSelectGenre = id => {
    let updateSelectedGenres = selectedGenres;
    let idx = selectedGenres.findIndex(genre => genre == id);
    if (idx == -1) {
      updateSelectedGenres.push(id);
    } else {
      updateSelectedGenres.splice(idx, 1);
    }
    setSelectedGenres(selectedGenres);
    setIsRefresh(!isRefresh);
  };

  const searchByGenres = movies => {
    let filteredMovies = movies.filter(movie => {
      let isSelected = true;
      for (let i = 0; i < selectedGenres.length; i++) {
        let idx = movie?.genre_ids?.findIndex(idx => idx == selectedGenres[i]);
        if (idx == -1) {
          isSelected = false;
          break;
        }
      }
      if (isSelected) {
        return movie;
      }
    });
    return filteredMovies;
  };

  const handleFilterMovie = movies => {
    let filteredMovies;

    if (selectedGenres.length > 0 && !searchTextInput) {
      filteredMovies = searchByGenres(movies);
    }
    if (searchTextInput && selectedGenres.length == 0) {
      filteredMovies = searchByTextInput(movies);
    }
    if (searchTextInput && selectedGenres.length > 0) {
      filteredMovies = searchByTextInput(searchByGenres(movies));
    }

    return filteredMovies;
  };

  const renderMoviePerSection = ({item}) => (
    <SectionMovie
      title={item.title}
      data={
        searchTextInput || selectedGenres.length > 0
          ? handleFilterMovie(item.data)
          : item.data
      }
      genres={genres}
      navigation={props.navigation}
    />
  );

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>MeoMee TV</Text>
        <TextInput
          style={styles.textInput}
          placeholder="Search Movie"
          value={searchTextInput}
          onChangeText={text => setSearchTextInput(text)}
        />
      </View>

      {!loading ? (
        <>
          <CardGenre genres={genres} handleSelectGenre={handleSelectGenre} />
          <View style={styles.movieTitle}>
            <FlatList
              data={movies}
              renderItem={renderMoviePerSection}
              keyExtractor={(item, index) => index}
              horizontal={false}
              showsVerticalScrollIndicator={false}
              extraData={isRefresh}
            />
          </View>
        </>
      ) : (
        <ActivityIndicator size="large" color="red" />
      )}
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: 'black',
    paddingBottom: 200,
  },
  title: {
    color: '#d81b60',
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 5,
  },
  movieTitle: {
    marginBottom: 20,
  },

  moviebyGenreContainer: {
    backgroundColor: 'white',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    height: 200,
    width: 300,
    marginHorizontal: 30,
    marginVertical: 10,
    borderRadius: 5,
  },
  textInput: {
    borderRadius: 10,
    width: '100%',
    backgroundColor: 'white',
  },
});
