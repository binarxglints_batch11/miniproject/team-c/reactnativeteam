import React, {useState, useEffect} from 'react';
import {Text, View, Image, TextInput} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getUserDetailAction} from '../action/userAction';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

import CustomButton from '../common/customButton';
import styles from '../styles/login';

const Login = props => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    dispatch(getUserDetailAction());
  }, []);

  const handleLogin = () => {
    // AsyncStorage.setItem('email', email);

    // console.log('email', email);
    // console.log('password', password);

    axios({
      method: 'POST',
      url: 'https://team-c.gabatch11.my.id/user/signin',

      data: {
        email,
        password,
      },
    })
      .then(({data}) => {
        AsyncStorage.setItem('accessToken', data.token, err => {
          if (!err) {
            setEmail('');
            setPassword('');
            props.navigation.navigate('Maintab');
          } else {
            alert(error.message);
          }
        });
      })
      .catch(err => {
        alert('email or password wrong', err);
      });
  };

  const handleRegister = () => {
    setEmail('');
    setPassword('');
    props.navigation.navigate('Register');
  };

  return (
    <View style={styles.container}>
      <Image source={require('../img/logo.png')} style={styles.logo} />
      <Text style={styles.logoText}>Meomee</Text>
      <View>
        <TextInput
          value={email}
          onChangeText={setEmail}
          style={styles.input}
          placeholder="Email"
          placeholderTextColor="white"
        />
      </View>
      <View>
        <TextInput
          value={password}
          onChangeText={setPassword}
          style={styles.input}
          placeholder="Password"
          placeholderTextColor="white"
          secureTextEntry
        />
      </View>
      {/* <Text style={styles.forget}>Forgot Your Password?</Text> */}

      <Text style={styles.dontHaveAccount} onPress={() => handleRegister()}>
        Dont Have an Account? Sign Up
      </Text>

      <View style={styles.buttonContainer}>
        <CustomButton title="Sign In" onPressButton={() => handleLogin()} />
      </View>
    </View>
  );
};

export default Login;
