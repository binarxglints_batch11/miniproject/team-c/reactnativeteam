import React, {useEffect} from 'react';
// Navigation
import {NavigationContainer} from '@react-navigation/native';

// Screen
import RootstackScreen from './src/common/RootStack';

// Store
import {Provider, useDispatch} from 'react-redux';
import store from './src/store';

const App = () => {
  // const dispatch = useDispatch();
  useEffect(() => {
    // dispatch (get...Action)()
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <RootstackScreen />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
